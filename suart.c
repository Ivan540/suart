/*
 * camera.c
 *
 *  Created on: 29 окт. 2018 г.
 *      Author: rovbuilder
 */

#define BAUDRATE 9600
#define  F_CPU 16000000L
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

//#  include <avr/iom328p.h>
#define BAUD 9600
#define UBRR_VAL F_CPU/16/BAUD-1

#define LED1 5
#define LED_PORT1 PORTB

#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)
#define FRAMING_ERROR (1<<FE0)
#define PARITY_ERROR (1<<UPE1)
#define DATA_OVERRUN (1<<DOR0)

// USART Receiver buffer
#define RX_BUFFER_SIZE 2
char rx_buffer[RX_BUFFER_SIZE];

unsigned char rx_wr_index,rx_rd_index,rx_counter;

// This flag is set on USART Receiver buffers overflow
char rx_buffer_overflow;
char read_enable = 0;
char flag;
long int zoom_out=0x0020000021;
long int zoom_in=0x0040000041;
long int zoom_step=0x0025000026;
char step=0;
long int stop=0x0000000001;
long int focus_in=0x0080000081;
long long int focus_out=0x0100000002;
//
//
void UARTSend(long long int data) {

	UDR0=0XFF;// SOH
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0=data>>8;
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0=data;
	_delay_ms(10);
}
void UARTSendChar(char data) {
	while(!(UCSR0A&(1<<UDRE0)));
		UDR0=data;
		while(!(UCSR0A&(1<<UDRE0)));
}

//
// ========================================================================================================================
// ========================================================================================================================



int i=0;

ISR (TIMER2_COMPA_vect)

{

	LED_PORT1=1<<3;
	_delay_us(30);
	LED_PORT1=0<<3;
}


ISR(USART_RX_vect)
{
	//UARTSendChar(0xEE);
	//LED_PORT1=1<<LED1;
	char status,data;
		status=UCSR0A;
		data=UDR0;
		LED_PORT1=0<<LED1;
		if ((status & (FRAMING_ERROR | DATA_OVERRUN))==0)
		{
			//UARTSendChar(data);
			if ((data == 0xBB)&&(rx_wr_index == 4)&&(read_enable == 0))
			{
			//	UARTSendChar(0xFF);
				rx_wr_index=0;
				read_enable = 1;

			}


			if (read_enable == 1)
			{
				rx_wr_index++;
				if (rx_wr_index == 2)
				{
						//rx_wr_index = 2;
						//read_enable=0;
						//flag=1;
						//UARTSend(data);
						OCR0A=(data);
						 }
				if (rx_wr_index == 3)
								{
										//rx_wr_index = 3;
										//read_enable=0;
										//flag=1;
										//UARTSend(data);
										OCR2A=(data);
										 }
				if (rx_wr_index == 4)
												{
														rx_wr_index = 4;
														read_enable=0;
														flag=1;
														//UARTSend(data);
														OCR1A=(data);
														 }

				}


			}

		}






void usart_init (unsigned int speed)
{
	// устанавливаем скорость Baud Rate: 4800
	UBRR0H=(unsigned char)(speed>>8);
	UBRR0L=(unsigned char) speed;
	UCSR0A=0x00;
	DDRB=0xFF;
	DDRD=0x00; //порт D работает как выход, так как к нему относятся выводы OC0A и OC0B

	UCSR0B|=(1<<TXEN0)|(1<<RXEN0);// Разрешение работы приемника
	UCSR0B|=(1<<RXCIE0);// Разрешение прерываний по приему
	// Установка формата посылки: 8 бит данных, 1 стоп-бит
	UCSR0C=(1<<UCSZ01)|(1<<UCSZ00);

}
/*void UARTSend(unsigned char data) {

	//UDR0=0XBB;// SOH
	//while(!(UCSR0A&(1<<UDRE0)));

//	UDR0=data>>8;
	while(!(UCSR0A&(1<<UDRE0)));
		UDR0=data;
	_delay_ms(10);
}
*/

void pwm_init(void)
{
    DDRB |= (1<<DDB1);
    PORTB|=(1<<PB4);
    OCR1A = 128;  // set PWM for 50% duty cycle
    TCCR1A=(1<<COM1A1)|(1<<WGM10); //На выводе OC1A единица, когда OCR1A==TCNT1, восьмибитный ШИМ
    TCCR1B=(1<<CS10);
}

void pwm_init1(void)
{
	DDRD |= (1 << DDD6);
	    // PD6 is now an output

	    OCR0A = 128;
	    // set PWM for 50% duty cycle


	    TCCR0A |= (1 << COM0A0);
	    // set none-inverting mode

	    TCCR0A |= (1 << WGM01) | (0 << WGM00);
	    // set fast PWM Mode

	    TCCR0B |= (1 << CS00)| (1 << CS02);
	    // set prescaler to 8 and starts PWM


}
void pwm_init2(void)
{
	DDRD |= (1 << DDD6);
	    // PD6 is now an output

	    OCR2A = 100;
	    // set PWM for 50% duty cycle
	    TIMSK2 |= (1<<OCIE2A);

	    TCCR2A |= (0 << COM2A0);
	    // set none-inverting mode

	    TCCR2A |= (1 << WGM21) | (0 << WGM20);
	    // set fast PWM Mode

	    TCCR2B |= (1 << CS21)| (1 << CS22)| (0 << CS20);
	    // set prescaler to 8 and starts PWM


}

void timer_init()
{
	//TCCR0A|=(0<<WGM00)|(0<<WGM01)|(1<<COM0A0)|(0<<COM0A1);
	//TCCR0B|=(0<<WGM02)|(1<<CS02)|(0<<CS01)|(1<<CS00);
}

int main(void)
{
	//DDRB = 1<<5;
	//DDRB = 1<<1;
	//PORTB = 1<<1;
//	LED_PORT1=0<<LED1;
//	DDRD=0b11111111;

	usart_init (UBRR_VAL);
	timer_init();
	pwm_init();
	pwm_init1();
	pwm_init2();
	sei();
	LED_PORT1=0<<LED1;
	flag=0;
	LED_PORT1=0<<LED1;
	rx_wr_index=4;
	read_enable=0;

	while(1)
		{

		//UARTSendChar(step);
		LED_PORT1=0<<LED1;
		_delay_ms(10);
		LED_PORT1=1<<LED1;
		_delay_ms(100);


		//_delay_ms(1000);
		//LED_PORT1=0<<LED1;
		//_delay_ms(100);
		//LED_PORT1=1<<LED1;
		//UARTSend(0xFF);


		}


}
